// [SECTION] CRUD Operations

    // CRUD Operation are the heart of any backend application
    // Mastering the CRUD Operation is essential for any developer.

//[SECTION] Inserting Documents (Create)

    // [SUBSECTION] Insert One Document
        // SInce MongoDB Deals wit object as it's structure for documents, we can easily create them by providing object into our methods.

        //Syntax:
            //db.collectionName.insertOne({ object });

db.users.insert({
    firstName: "Jane",
    lastName: "Doe",
    age: 12,
    contact: {
        phone: "87654321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
});

//[SUB-SECTION] Insert Many Document
    //Syntax:
        // db.collectionName.insertMany([{objectA}, {objectB}, ... ]})

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "987654321",
            email: "theoryofeverthing@mail.com",
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "987654321",
            email: "neiltothemoon@mail.com",
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

// [SECTION] Finding Documents (READ)
    // If multiple documents match the criteria for finding a document that match the search term will be returned 

    //Syntax
        //db.collectionName.find()
        //db.collectionName.find({field: value})

db.users.find()
db.users.find({firstName: "Stephen"})


// [SECTION] Updating document (UPDATE)
    // [SUB-SECTION] Updating a Single Document
    //syntax
        // db.collectionName.updateOne({criteria}, {$set: {field: value}})

    //Creat a document to update
db.users.insert({
    firstName: "Test",
    lastName: "Test",
    age: 82,
    contact: {
        phone: "987654321",
        email: "neiltothemoon@mail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none"

});

    // To update the firstName: "Test"


db.users.updateOne({firstName: "Test"},
    {
    $set : {
        firstName: "Bill",
        lastName: "Gates",
        age: 82,
        contact: {
            phone: "987654321",
            email: "billgates@mail.com",
        },
        courses: ["React", "Laravel", "Sass"],
        department: "Operations",
        status: "Active"
    }
});

//[SUB-SECTION] Update Many
    //syntax
        // db.collectionName.updateMany({criteria}, {$set: {field: value}})

db.users.updateMany({department: "none"}, {
    $set: {
        department: "HR"
    }
});


// [ SECTION ] Deleting Documents (DELETE)

	// Syntax
		// db.collectionName.deleteOne({ criteria });
db.user.insert({
	firstName: "test"
});
	
	// [ SUB-SECTION ] Delete One

	db.user.deleteOne({
	firstName: "test"
});

db.users.deleteOne({
	department: "Accounting"
});

	// [ SUB-SECTION ] Delete Many
	
	// Syntax: 
		// db.collectionName.deleteMany({ criteria });

db.users.deleteMany({
	department: "none"
});
